package ru.ovechkin.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * http://localhost:8888/app-test.properties
 */
@EnableConfigServer
@SpringBootApplication
public class ServerConfig {

    public static void main(String[] args) {
        SpringApplication.run(ServerConfig.class, args);
    }

}