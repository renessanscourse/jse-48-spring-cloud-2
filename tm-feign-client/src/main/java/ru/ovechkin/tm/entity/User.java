package ru.ovechkin.tm.entity;

import ru.ovechkin.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class User extends AbstractEntity {

    @Nullable
    private List<Project> projects = new ArrayList<>();

    @Nullable
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private Role role;

    private boolean locked = false;

    public User() {
    }

    public User(@NotNull String login, @NotNull String passwordHash, @NotNull Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    @Nullable
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(@Nullable List<Project> projects) {
        this.projects = projects;
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @Nullable
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@NotNull String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

    public boolean getLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

}
