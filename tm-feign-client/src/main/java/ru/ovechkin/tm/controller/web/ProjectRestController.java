package ru.ovechkin.tm.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.client.IProjectRestController;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

@RestController
@RequestMapping("/api/rest/projects")
public class ProjectRestController {

    @Autowired
    private IProjectRestController projectRestController;

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> allProjects() {
        return projectRestController.allProjects();
    }

    @PutMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> create(@RequestBody Project project) {
        return projectRestController.create(project);
    }

    @DeleteMapping(value = "/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> remove(
            @RequestParam("projectId") String projectId
    ) {
        return projectRestController.remove(projectId);
    }

    @PostMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> edit(
            @RequestParam("id") final String projectId,
            @RequestBody final Project project
    ) {
        return projectRestController.edit(projectId, project);
    }

}