package ru.ovechkin.tm.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ovechkin.tm.client.IAuthenticationRestController;

@RestController
@RequestMapping("/api/rest/authentication")
public class AuthenticationRestController {

    @Autowired
    private IAuthenticationRestController authenticationRestController;

    @GetMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public String showPort() {
        return authenticationRestController.showPort();
    }

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        return authenticationRestController.login(username, password);
    }

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public ru.ovechkin.tm.entity.User profile() {
        return authenticationRestController.profile();
    }

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public String logout() {
        return authenticationRestController.logout();
    }

}