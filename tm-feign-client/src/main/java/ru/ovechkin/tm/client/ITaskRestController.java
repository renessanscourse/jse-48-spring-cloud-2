package ru.ovechkin.tm.client;

import feign.Client;
import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.entity.Task;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

@RequestMapping("/rest/tasks")
@FeignClient(name = "app-server-enterprise")
public interface ITaskRestController {

//    String URL = "http://localhost:8080/";
//
//    static ITaskRestController client(final Client okHttpClient) {
//        return client(URL, okHttpClient);
//    }
//
//    static ITaskRestController client(final String baseUrl, final Client okHttpClient) {
//        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
//        final HttpMessageConverters converters = new HttpMessageConverters(converter);
//        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
//        if (okHttpClient == null) {
//            final CookieManager cookieManager = new CookieManager();
//            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
//
//            final okhttp3.OkHttpClient.Builder builder =
//                    new okhttp3.OkHttpClient().newBuilder();
//            builder.cookieJar(new JavaNetCookieJar(cookieManager));
//
//            return Feign.builder()
//                    .client(new OkHttpClient(builder.build()))
//                    .contract(new SpringMvcContract())
//                    .encoder(new SpringEncoder(objectFactory))
//                    .decoder(new SpringDecoder(objectFactory))
//                    .target(ITaskRestController.class, baseUrl);
//        } else {
//            return Feign.builder()
//                    .client(okHttpClient)
//                    .contract(new SpringMvcContract())
//                    .encoder(new SpringEncoder(objectFactory))
//                    .decoder(new SpringDecoder(objectFactory))
//                    .target(ITaskRestController.class, baseUrl);
//        }
//    }

    @GetMapping(value = "/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> show(@PathVariable("projectId") String projectId);

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> create(
            @RequestParam("projectId") String projectId,
            @RequestBody Task task
    );

    @DeleteMapping(value = "/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> remove(
            @RequestParam("taskId") String taskId,
            @RequestParam("projectId") String projectId
    );

    @PatchMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> edit(
            @RequestBody Task task,
            @RequestParam("projectId") String projectId,
            @RequestParam("taskId") String taskId
    );

}
