package ru.ovechkin.tm.client;

import feign.Client;
import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.CookieManager;
import java.net.CookiePolicy;

@RequestMapping("/rest/authentication")
@FeignClient(name = "app-server-enterprise")
public interface IAuthenticationRestController {

//    String URL = "http://localhost:8080/";
//
//    static IAuthenticationRestController client(final Client client) {
//        return client(URL, client);
//    }
//
//    static IAuthenticationRestController client(final String baseUrl, final Client okHttpClient) {
//        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
//        final HttpMessageConverters converters = new HttpMessageConverters(converter);
//        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
//
//        if (okHttpClient == null) {
//            final CookieManager cookieManager = new CookieManager();
//            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
//
//            final okhttp3.OkHttpClient.Builder builder =
//                    new okhttp3.OkHttpClient().newBuilder();
//            builder.cookieJar(new JavaNetCookieJar(cookieManager));
//
//            return Feign.builder()
//                    .client(new OkHttpClient(builder.build()))
//                    .contract(new SpringMvcContract())
//                    .encoder(new SpringEncoder(objectFactory))
//                    .decoder(new SpringDecoder(objectFactory))
//                    .target(IAuthenticationRestController.class, baseUrl);
//        } else {
//            return Feign.builder()
//                    .client(okHttpClient)
//                    .contract(new SpringMvcContract())
//                    .encoder(new SpringEncoder(objectFactory))
//                    .decoder(new SpringDecoder(objectFactory))
//                    .target(IAuthenticationRestController.class, baseUrl);
//        }
//    }

    @GetMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    String showPort();

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    String login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password);

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    ru.ovechkin.tm.entity.User profile();

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    String logout();

}

