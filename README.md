# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: OVECHKIN ROMAN

**E-MAIL**: ovechkin@roman.ru

# SOFTWARE

- JDK 1.8

- Windows 10

# PROGRAM BUILD

```bash
mvn clean install
```

# SCREENSHOTS

https://disk.yandex.ru/d/InFf435WVJYcWQ?w=1