package ru.ovechkin.tm.exeption.empty;

public class ProjectIdEmptyException extends RuntimeException {

    public ProjectIdEmptyException() {
        super("Error! You need a Project ID to create a task and attach it to project...");
    }

}