package ru.ovechkin.tm.exeption.user;

public class AdminRemovingException extends RuntimeException{

    public AdminRemovingException() {
        super("Error! You can't remove an admin...");
    }

}
