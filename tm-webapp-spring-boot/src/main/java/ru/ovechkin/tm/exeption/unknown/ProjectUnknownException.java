package ru.ovechkin.tm.exeption.unknown;

public class ProjectUnknownException extends RuntimeException {

    public ProjectUnknownException() {
        super("Error! This Project does not exist.");
    }

    public ProjectUnknownException(final String name) {
        super("Error! This Project [" + name + "] does not exist.");
    }

}
