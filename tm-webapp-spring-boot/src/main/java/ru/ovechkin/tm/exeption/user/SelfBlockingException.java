package ru.ovechkin.tm.exeption.user;

public class SelfBlockingException extends RuntimeException {

    public SelfBlockingException() {
        super("Error! You can't lock yourself...");
    }

}
