package ru.ovechkin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.dto.CustomUser;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.IdEmptyException;
import ru.ovechkin.tm.exeption.empty.ProjectEmptyException;
import ru.ovechkin.tm.exeption.empty.ProjectIdEmptyException;
import ru.ovechkin.tm.exeption.empty.UserEmptyException;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.unknown.ProjectUnknownException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.UserRepository;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Project> findAllUserProjects(@Nullable final CustomUser user) {
        if (user == null) throw new NotLoggedInException();
        return projectRepository.findAllByUserId(user.getUserId());
    }

    @Override
    @Transactional
    public void save(@Nullable final Project project, @Nullable final CustomUser user) {
        if (user == null) throw new NotLoggedInException();
        if (project == null) throw new ProjectUnknownException();
        project.setUser(userRepository.findByLogin(user.getUsername()));
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String projectId, @Nullable final CustomUser user) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (user == null) throw new NotLoggedInException();
        @Nullable final Project project =
                projectRepository.findById(projectId).orElse(null);
        if (project == null) throw new ProjectUnknownException(projectId);
        if (!project.getUser().getId().equals(user.getUserId())) throw new AccessDeniedException();
        projectRepository.deleteById(projectId);
    }

    @Override
    public Project findById(@Nullable final String projectId, @Nullable final CustomUser user) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (user == null) throw new NotLoggedInException();
        @Nullable final Project project =
                projectRepository.findById(projectId).orElse(null);
        if (project == null) throw new ProjectUnknownException(projectId);
        if (!user.getUserId().equals(project.getUser().getId())) throw new AccessDeniedException();
        return project;
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String projectId,
            @Nullable final Project project,
            @Nullable final CustomUser user
    ) {
        if (project == null) throw new ProjectEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (user == null) throw new NotLoggedInException();
        @Nullable final Project projectToUpdate =
                projectRepository.findById(projectId).orElse(null);
        if (projectToUpdate == null) throw new ProjectUnknownException(projectId);
        if (!projectToUpdate.getUser().getId().equals(user.getUserId())) throw new AccessDeniedException();
        projectToUpdate.setName(project.getName());
        projectToUpdate.setDescription(project.getDescription());
        projectRepository.save(projectToUpdate);
    }

}