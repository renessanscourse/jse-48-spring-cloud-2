package ru.ovechkin.tm.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ovechkin.tm.api.service.IUserService;

import javax.annotation.Resource;

@RestController
@RequestMapping("/rest/authentication")
public class AuthenticationRestController {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

// Это нужно для теста смещения по портам
//    @Value("${server.port}")
//    private String port;
//
//    @GetMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
//    public String showPort() {
//        return "CURRENT PORT IS : [ " + port + " ]";
//    }

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication =
                authenticationManager.authenticate(token);
        System.out.println(token.getCredentials().toString());
        System.out.println(token.getPrincipal().toString());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return ("success status: " + authentication.isAuthenticated());
    }

    @GetMapping(value = "/session", produces = MediaType.APPLICATION_JSON_VALUE)
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public User user(@AuthenticationPrincipal(errorOnInvalidType = true) final User user) {
        return user;
    }

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public ru.ovechkin.tm.entity.User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userService.findByLogin(username);
    }

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public String logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return "success";
    }

}