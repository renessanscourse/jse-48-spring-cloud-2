package ru.ovechkin.tm.service;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.exeption.unknown.TaskUnknownException;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.rest.controller.AuthenticationRestController;
import ru.ovechkin.tm.util.UserUtil;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class TaskServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private AuthenticationRestController authenticationRestController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ITaskService taskService;

    private final Project project = new Project();

    {
        project.setName("testTask");
        project.setDescription("testTask");
    }

    final Task task = new Task(); {
        task.setProject(project);
        task.setName("test");
        task.setDescription("test");
    }


    @Before
    public void setUp() throws Exception {
        final String login = "test_user";
        final String password = "test_user";
        final User user = new User();
        user.setId("constantId");
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setRole(Role.USER);
        userRepository.save(user);
        authenticationRestController.login(login, password);
        projectService.save(project, UserUtil.getUser());
    }

    @Test
    public void testFindAll() {
        taskService.save(task, UserUtil.getUser());
        Assert.assertEquals(1, taskService.findAll(project.getId(), UserUtil.getUser()).size());
    }

    @Test
    public void testSave() {
        taskService.save(task, UserUtil.getUser());
        Assert.assertEquals(
                task.toString(),
                taskService.findById(task.getId(), UserUtil.getUser()).toString());
    }

    @Test
    public void testRemoveById() {
        taskService.save(task, UserUtil.getUser());
        Assert.assertEquals(
                task.toString(),
                taskService.findById(task.getId(), UserUtil.getUser()).toString());
        taskService.removeById(task.getId(), UserUtil.getUser());
        Assert.assertThrows(
                TaskUnknownException.class,
                () -> taskService.findById(task.getId(), UserUtil.getUser()));
    }

    @Test
    public void testFindById() {
        taskService.save(task, UserUtil.getUser());
        Assert.assertEquals(
                task.toString(),
                taskService.findById(task.getId(), UserUtil.getUser()).toString());
    }

    @Test
    public void testUpdateById() {
        taskService.save(task, UserUtil.getUser());
        Assert.assertEquals(
                task.toString(),
                taskService.findById(task.getId(), UserUtil.getUser()).toString());
        final Task taskUpdated = new Task();
        taskUpdated.setName("EDITED");
        taskUpdated.setDescription("EDITED");
        taskService.updateById(task.getId(), taskUpdated, UserUtil.getUser());
    }

}