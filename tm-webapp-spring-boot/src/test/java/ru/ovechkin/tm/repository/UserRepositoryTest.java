package ru.ovechkin.tm.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testSave() {
        final User user = new User();
        user.setLogin("TEST_SAVE");
        user.setPasswordHash("TEST_SAVE");
        user.setRole(Role.USER);
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
    }

    @Test
    public void testFindById() {
        final User user = new User();
        user.setLogin("TEST_FIND_ID");
        user.setPasswordHash("TEST_FIND_ID");
        user.setRole(Role.USER);
        userRepository.save(user);
        assertEquals(user.toString(), userRepository.findById(user.getId()).get().toString());
    }

    @Test
    public void testDeleteById() {
        final User user = new User();
        user.setLogin("TEST_DELETE");
        user.setPasswordHash("TEST_DELETE");
        user.setRole(Role.USER);
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
        userRepository.deleteById(user.getId());
        assertEquals(Optional.empty(), userRepository.findById(user.getId()));
    }

    @Test
    public void testUpdate() {
        final User user = new User();
        user.setLogin("TEST_TO_CHANGE");
        user.setPasswordHash("TEST_TO_CHANGE");
        user.setRole(Role.USER);
        userRepository.save(user);
        assertEquals(user.toString(), userRepository.findById(user.getId()).get().toString());
        user.setLogin("TEST_CHANGED");
        userRepository.save(user);
        assertEquals(user.toString(), userRepository.findById(user.getId()).get().toString());
    }

}
