package ru.ovechkin.tm.controller;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.User;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class UserControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();
    }

    @Test
    public void testGetRegistryForm() throws Exception {
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/user/registryForm"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("registry-form"));
    }

    @Test
    public void testRegister() throws Exception {
        final User user = new User();
        user.setLogin("testRegister");
        user.setPasswordHash("testRegister");
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/user/register")
                        .flashAttr("user", user))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }

}